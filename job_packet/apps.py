from django.apps import AppConfig


class JobPacketConfig(AppConfig):
    name = 'job_packet'
